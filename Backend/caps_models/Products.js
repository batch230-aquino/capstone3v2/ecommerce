const mongoose = require("mongoose");


const productSchema = new mongoose.Schema({
   productName:{
    type: String,
    required:[true, "Name is required"]
   },
   description:{
    type: String,
    required:[true, "Description is required"]
   },
   price:{
    type:Number,
    required:[true, "Price is required"]
   },
   stocks:{
    type: Number,
    required:[true, "Stocks is required"]
   },
   isActive:{
    type: Boolean,
    default: true
   }
    

})

module.exports = mongoose.model("Products", productSchema);