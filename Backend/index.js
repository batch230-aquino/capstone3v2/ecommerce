const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./caps_routes/userRoutes");
const productRoutes = require("./caps_routes/productRoutes");
const orderRoutes = require("./caps_routes/orderRoutes");


const app = express ();

app.use(cors());

app.use(express.json());

app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://admin:admin@batch230.vsenssp.mongodb.net/AquinoECommerce?retryWrites=true&w=majority", 
{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the Ecommerce database."));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);


app.listen(process.env.PORT || 4000, () =>{
    console.log(`API is now online ${process.env.PORT || 4000}`)
})
