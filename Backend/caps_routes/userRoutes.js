const express = require("express");
const router = express.Router();
const userControllers = require("../caps_controllers/userControllers");
const auth = require("../auth");

router.get("/all", userControllers.getAllUsers);

router.post("/registration", userControllers.userRegistration);

router.post("/login", userControllers.userLogin);

router.patch("/update/:userId", auth.verify, userControllers.updateUser);

router.post("/emailChecker", userControllers.isEmailExisting);

router.get("/getData", userControllers.getUserDetails);

router.get("/editData/:userId", userControllers.getData)





module.exports = router;