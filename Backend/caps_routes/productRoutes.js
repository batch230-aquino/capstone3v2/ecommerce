const express = require("express");
const router = express.Router();
const productControllers = require("../caps_controllers/productControllers");
const auth = require("../auth");


router.post("/add", auth.verify, productControllers.addProducts);

router.get("/getAll", auth.verify, productControllers.getAll);

router.get("/",  productControllers.getActiveItems);

router.get("/:productId", productControllers.specificItem);

router.patch("/:productId/update", auth.verify, productControllers.updateItem);

router.patch("/archive/:productId", auth.verify, productControllers.saveProduct);




module.exports = router;