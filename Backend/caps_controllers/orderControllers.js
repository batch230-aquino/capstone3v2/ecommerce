const express = require("express");
const Orders = require("../caps_models/orders");
const Users = require("../caps_models/Users");
const Products = require("../caps_models/Products");
const auth = require("../auth");


// new test with added function
module.exports.addToCart = async (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    const userId = userData.id;

    const product = await Products.findById(req.body.productId);

    const productStocks = product.stocks;

    let orderQuantity = req.body.quantity;

    let newStock = productStocks - orderQuantity;

    if (newStock < 0) {
      return res.send(false);
    }

    const order = new Orders({
        userId: userId,
        totalAmount: product.price,
        products: [{
        productId: product._id,
        productName: product.productName,
        quantity: orderQuantity
        }]
    });

    product.stocks = newStock;
    await product.save();
    await order.save();

    return res.status(200).send(true);
//     return res.status(200).send({
//       message: 'Order successfully placed',
//       order: order
// });
  } 
    catch (error) {
    console.log(error);
    return res.status(500).send(false);
  }
}





//Cart of allunpaid orders
module.exports.allUnpainOrders = (req, res) =>{
   
const userData = auth.decode(req.headers.authorization);

let subTotal = []


  return Orders.find({userId:userData.id, isPaid:false})
  .then(result =>{
    result.forEach(perResult =>{
      console.log(perResult.totalAmount)

      subTotal.push(perResult.totalAmount)
      console.log(subTotal)   

      
    })

    let sumOfSubTotal = subTotal.reduce((x, y)=>{
      return x + y;
    });
    console.log(sumOfSubTotal);

    res.send(`${result}\n All Pending Product Total Price:${sumOfSubTotal}`);
    
    
  })
  .catch(error =>{
    console.log(error);
    res.send(false)
  })
};


//all orders retrieval//

module.exports.checkOutOrders = (req, res) =>{
   
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
      return Orders.find({isPaid:true})
      .then(result =>{
        console.log(result);
        res.send(result);
      })
      .catch(error =>{
        console.log(error);
        res.send("Error to retrieve all orders")
      })
    }
    else{
      console.log("Only admin has the access to this function.")
    }
};

//update orders (admin only) Pending

module.exports.updatingCart = (req, res) =>{
  
  const  userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin){
    return Orders.findByIdAndUpdate(req.params.orderId, {isPaid:req.body.isPaid})
    .then(result =>{
      console.log(result);
      res.send("updated paid checkout")
    })
    .catch(error =>{
      console.log(error);
      res.send("failed to update")
    })
    }

  }







//retrieve auth user's order

module.exports.userOrders = (req, res) => {
  
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
      return Orders.findById(req.body.orderId)
      .then(result =>{
        console.log(result);
        res.send(result)
      })
      .catch(error =>{
        console.log(error);
        res.send("Order Id not found.")
      })

    }
    else{
      console.log("Only Admin has the access to this function.")
  
  }
    
}

//Delete One item

module.exports.DeleteOrder = (req, res) => {

  const userData = auth.decode(req.headers.authorization);

  return Orders.findByIdAndDelete(req.params.orderId)
  .then(result => res.send("Product Deleted"))
  .catch(error => res.send(error))
}

//Delete All

module.exports.DeleteAllUnpaid = (req, res) => {

  const userData = auth.decode(req.headers.authorization)

  return Orders.deleteMany({isPaid: false})
  .then(result => res.send(true))
  .catch(error => res.send(error))
}
