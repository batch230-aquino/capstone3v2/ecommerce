const Products = require("../caps_models/Products");
const auth = require("../auth");


//Only Admin can access
module.exports.addProducts = (req, res) =>
{
    const  userData = auth.decode(req.headers.authorization);


    let newProducts = new Products({
        productName: req.body.productName,
        description: req.body.description,
        price:req.body.price,
        stocks: req.body.stocks
    });

    if(req.userData.isAdmin){
        
        return newProducts.save()
        .then(products =>{
            console.log(products)
            res.send(true)
        })
        .catch(error =>{
            console.log(error);
            res.send(false)
        })
    }
    else{
        return res.status(404).send(false);
    }
};

//retrieve all items

module.exports.getAll = (req, res)=>{

    const userData = auth.decode(req.headers.authorization);
    
    if(req.userData.isAdmin){
        return Products.find({})
        .then(result => res.send(result));
    }
    else{
        return res.send(false);
    }
   
}

//ALl active Items
module.exports.getActiveItems = (req, res) =>{
    return Products.find({isActive: true})
    .then(result => res.send(result));
}

//retrieve single items
module.exports.specificItem = (req, res) =>{
    console.log(req.params.productId);
    return Products.findById(req.params.productId).then(result => res.send(result));
}

//Update product information
module.exports.updateItem = (req, res) =>{
    const  userData = auth.decode(req.headers.authorization);
    if(req.userData.isAdmin){
        let updatedItem = {
            productName: req.body.productName,
            description: req.body.description,
            price: req.body.price,
            stocks: req.body.stocks
        }

        return Products.findByIdAndUpdate(req.params.productId, updatedItem, {new:true})
        .then(result =>{ 
            console.log(result);
            res.send(true);
        })
        .catch(error =>{
            console.log(error);
            res.send(false);
        })
    }
};

//Archive product

module.exports.saveProduct = (req, res) =>{
    const userData = auth.decode(req.headers.authorization);

    let updateIsActiveField = {
		isActive: req.body.isActive
	}

    if(userData.isAdmin){
        return Products.findByIdAndUpdate(req.params.productId, updateIsActiveField)
        .then(result => {
            console.log(result);
             res.send(true)
        })
        .catch(error => {
            console.log(error);
            res.send(false);
        });
    }
    else{
        return res.send(false);
    }
}



