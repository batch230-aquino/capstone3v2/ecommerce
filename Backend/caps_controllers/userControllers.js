const User = require("../caps_models/Users");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Products = require("../caps_models/Products");
const Orders = require("../caps_models/orders");

// registration
module.exports.userRegistration = (req, res) =>
{
    let newUser = new User ({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNo: req.body.mobileNo
    })

    console.log(newUser);

    return newUser.save()

    .then(user => {
        console.log(user);
        res.send(true);
    })
    .catch(error => {
        console.log(error);
        res.send(false)
    })
}

    

//login

module.exports.userLogin = (req, res) =>{
    return User.findOne({email: req.body.email})
    .then(result =>{
        if(result == null){
            return res.send(false)
        }
        else{
            const isPasswordMatch = bcrypt.compareSync(req.body.password, result.password);

            if(isPasswordMatch){
                
                return res.send({accessToken: auth.createAccessToken(result)});
            }
            else{
                
                return res.send(false)
            }
        }
    })
}

//Email verifier

module.exports.isEmailExisting = (req, res) =>{
    return User.find({email: req.body.email})
    .then(result => {
        console.log(result);
      
        if(result.length > 0){
            return res.send(true);
        }
        else{
            return res.send(false);
        }
    })
    .catch(error => res.send(error));
}


//userUpdate

module.exports.updateUser = (req, res) =>{

    const  userData = auth.decode(req.headers.authorization);
    
        if(req.userData.isAdmin){
            let updatedUser = {
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
              isAdmin: req.body.isAdmin,
              mobileNo: req.body.mobileNo
            }   

        return User.findByIdAndUpdate(req.params.userId, updatedUser, {new:true})
        .then(result =>{
            console.log(result);
            res.send(true);
        })
        .catch(error =>{
            console.log(error);
            res.send(false);
        })
    }
};

//retrieve usersdata
// module.exports.getUserDetails = (req, res) =>{

//     const userData = auth.decode(req.headers.authorization);
//     if(userData.isAdmin){

//         return User.findById(req.params.userId).then(result => {
//             result.password = '';
//             res.send(result);
//         }).catch(error =>{
//             res.send(false)
//         })
//     }
    
// }
//Specific user
module.exports.getUserDetails = (req, res) => {
    
    
		const userData = auth.decode(req.headers.authorization);
            console.log(userData)
    return User.findById(userData.id).then(result =>{
        result.password = "***";
        res.send(result);
    })
};


//Specific user for admindash
module.exports.getData = (req, res) => {
    
    
    const userData = auth.decode(req.headers.authorization);
        console.log(userData)
return User.findById(req.params.userId).then(result =>{
    result.password = "***";
    res.send(result);
})
};

//retreive all userdata
// module.exports.getAllUser = (req, res)=>{
//     const userData = auth.decode(req.headers.authorization);

//     if(userData.isAdmin){
        
//         return User.find({isAdmin:false})
//     .then(result => res.send(result))
//     .catch(error =>{
//         console.log(error);
//         res.send(error);
//     })
//     }
    
// }

module.exports.getAllUsers = (req, res) =>{
	
	
	return User.find({}).then(result => res.send(result));
	
	
};









   


