import {Row, Col, Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import {Fragment} from 'react';
import Container from 'react-bootstrap/Container';




export default function Banner({data}){

    console.log(data)

    const {title, content, destination, label} = data;

    return(
        <div className="homebackground">
        <Row>
            <Col className='p-5 text-center'>
                <h1>{title}</h1>
                <p>{content}</p>
                <Link to = {'/products'}>{label}</Link>
            </Col>
        </Row>

        <Container>
        <Carousel fade>
      <Carousel.Item className='home-carousel1'>
        <img
          className="d-block w-75 mx-auto"
          src="https://m.media-amazon.com/images/W/IMAGERENDERING_521856-T1/images/I/81aIviy7YZL._AC_SL1500_.jpg"
          alt="First slide"
        />
       
      </Carousel.Item>
      <Carousel.Item className='home-carousel'>
        <img
          className="d-block w-75 mx-auto"
          src="https://omggamer.com/wp-content/uploads/2022/01/NVIDIA-GeForce-RTX-4000.png"
          alt="Second slide"
        />

       
      </Carousel.Item>
      <Carousel.Item className='home-carousel'>
        <img
          className="d-block w-75 mx-auto"
          src="https://storage-asset.msi.com/global/picture/news/2019/case/RGB-Lighting.jpg"
          alt="Third slide"
        />

        
      </Carousel.Item>
    </Carousel>
    </Container>
     </div>

        
    


    )
}