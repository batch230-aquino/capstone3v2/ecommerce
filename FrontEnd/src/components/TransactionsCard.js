import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';





export default transactionsCard;

function transactionsCard({transactionProp}) {
console.log(transactionProp);


    const {_id, userId, purchaseOn, totalAmount, products } = transactionProp;

return (
    <Card className="transaction">

    <Card.Header>userId: {userId}</Card.Header>

    <Card.Body>
    <Card.Title>Transaction ID: {_id}</Card.Title>
        <Card.Title>Total Amount: ₱{totalAmount}</Card.Title>
        <Card.Text>
        Date Purchased: {purchaseOn}
        </Card.Text>

        <Card.Text>
        Product Name: {products[0].productName}
        </Card.Text>

        
      </Card.Body>
      <Card.Footer className="text-muted">2 days ago</Card.Footer>
    </Card>
  );
}