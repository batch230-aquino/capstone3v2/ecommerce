import { useContext, useState, useEffect } from "react";
import UserContext from "../UserContext";
import TransactionsCard from "../components/TransactionsCard";



export default function Transactions(){

const { user } = useContext(UserContext)

        console.log(user);

    const [orders, setOrders] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`,{
            headers:{
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setOrders(data.map(order  =>{
                console.log("list of orders:")
                console.log(order)
                return(
                    <TransactionsCard key={order._id} transactionProp={order} />
                )
            }))
        })
    }, []);


    return(
        <>
        <h1 className="text-center mt-3 mb-5">Transaction History</h1>
            {orders}
        </>
        
    )
}